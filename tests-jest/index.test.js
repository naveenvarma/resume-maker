const { base, send_message, resumes, resume } = require('../controllers/index.controller');
const { MessagesDAO } = require('../models/dao/messages_dao');


jest.mock("../models/dao/messages_dao");
jest.mock("../models/dao/resumes_dao");

// Import the mocked modules
const mockedMessagesDAO = require("../models/dao/messages_dao");
const mockedResumesDAO = require("../models/dao/resumes_dao");

describe('Index Route Handlers', () => {
    afterEach(() => {
        jest.clearAllMocks(); // Clear mock function calls after each test
    });
    
    test('GET / should call base controller and render index view with correct title', async () => {
      // Mock request, response, and next objects
        const req = {};
        const res = {
            render: jest.fn()
        };
        const next = {};

        // Call the base controller function
        await base(req, res, next);

        expect(res.render).toHaveBeenCalledWith('index', { title: 'Express' }); // Ensure that res.render is called with correct arguments
    });

    test('should respond with success message if message is sent successfully', async () => {
        const req = { body: {} };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn()
        };

        mockedMessagesDAO.prototype.insert.mockResolvedValueOnce({ acknowledged: true, insertedId: 'someId' });

        await send_message(req, res); 

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({ status: 'success', message: 'Message sent successfully.' });
    });

    test('should respond with faliure message if message is not sent successfully', async () => {
        const req = { body: {} };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn()
        };

        mockedMessagesDAO.prototype.insert.mockResolvedValueOnce({ acknowledged: false, insertedId: 'someId' });

        await send_message(req, res); 

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledWith({ status: 'error', message: 'Failed to send message.' });
    });

    test('should respond with error message if message is not sent successfully', async () => {
        const req = { body: {} };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn()
        };
    
        mockedMessagesDAO.prototype.insert.mockRejectedValueOnce(new Error("Failed to send message."));
    
        await send_message(req, res); 
    
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({ status: 'error', message: 'Internal server error.' });
    });

});
