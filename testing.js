function simulate(nums, left, right){
    indexes_to_change = []
    for(let i=0;i<nums.length;i++){
        if(i-left>=0){
            if(nums[i]<=nums[i-left]){
                indexes_to_change.push(i);
                continue;
            }
        }
        if(i+right<nums.length){
            if(nums[i]<=nums[i+right]){
                indexes_to_change.push(i);
            }
        }
    }
    indexes_to_change.forEach(index => {
        nums[index]=0;
    });
    return nums;
}

nums = [1, 2, 0, 5, 0, 2, 4, 3, 3, 3]
console.log(simulate(nums,3,4));